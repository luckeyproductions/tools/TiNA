![TiNA Logo](Logo.png)

## Transfer Normals Add-on for Blender

T!NA allows you to quickly transfer normals and (optionally) vertex colours between near vertices of different objects. These objects can then be seamlessly assembled while remaining separate.

### Instructions

#### Installation

Download the [.py file](TiNA.py) and in the add-ons tab of Blender's user preferences click _Install Add-on from File_. After locating and installing the add-on it can be activated by checking the box before its name in the list of add-ons. Be sure to save the user settings if you want the add-on to be enabled by default.

#### Hotkeys

|                 | Operation |
|-----------------|-----------|
| **Alt+N**       | Transfer normals from selection to active object |
| **Shift+Alt+N** | Transfer normals from active object to all other selected objects |
| **Ctrl+Shift+Alt+N** | Clear custom normals data for entire selection |
| **Alt+W** | Wrap normals |


### Todo

- _Fuse_ operation
- Add to Object menu